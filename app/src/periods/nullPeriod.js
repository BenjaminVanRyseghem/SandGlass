class NullPeriod {
	periodIndex() {
		return "null";
	}

	isNull() { return true; }

	getYears() { return [this]; }

	getMonths() { return [this]; }

	getWeeks() { return [this]; }

	getDays() { return [this]; }

	accept(visitor) {
		return visitor.visitNullPeriod(this);
	}
}

module.exports = new NullPeriod();
