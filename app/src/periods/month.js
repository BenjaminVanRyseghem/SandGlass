const Period = require("./period");

class Month extends Period {
	constructor({ year, weeks }) {
		super(...arguments); // eslint-disable-line prefer-rest-params

		this._year = year;
		this._weeks = weeks;

		for (let week of this._weeks) {
			week.setMonth(this);
		}
	}

	getName() {}

	// To use only once to set the back pointer
	setYear(year) {
		if (this._year) {
			throw new Error("`this._year` has already been set!");
		}

		this._year = year;
	}

	getYear() {
		return this._year;
	}

	getMonths() {
		return [this];
	}

	getWeeks() {
		return this._weeks;
	}

	getDays() {
		return this.getWeeks().reduce((acc, week) => acc.concat(week.getDays()), []);
	}

	accept(visitor) {
		return visitor.visitMonth(this);
	}

	containsMonth(otherMonth) {
		return otherMonth === this.periodIndex();
	}
}

module.exports = Month;
