const moment = require("moment");

class Helper {
	getMonthIndex(data) {
		return +moment(data).format("M");
	}

	getWeekIndex(data) {
		return +moment(data).format("W");
	}

	getYearIndex(data) {
		return +moment(data).format("YYYY");
	}

	getDayIndex(data) {
		return +moment(data).format("DDD");
	}

	isWeekBroken(weekNo, year) {
		let week = this.getDateRangeOfWeek(weekNo, year);
		let { start } = week;
		let { end } = week;
		return start.getMonth() !== end.getMonth();
	}

	// From https://gist.github.com/Abhinav1217/5038863
	getDateRangeOfWeek(weekNo, year) {
		if (weekNo < 1) {
			throw new Error("`weekNo` must be greater or equal to 1");
		}

		let date = new Date(`${year}-02-03`);

		let numOfdaysPastSinceLastMonday = date.getDay() - 1;
		date.setDate(date.getDate() - numOfdaysPastSinceLastMonday);

		let weekNoToday = this.getWeekIndex(date);
		let weeksInTheFuture = weekNo - weekNoToday;
		date.setDate(date.getDate() + 7 * weeksInTheFuture);

		return {
			start: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
			end: new Date(date.getFullYear(), date.getMonth(), date.getDate() + 6)
		};
	}

	getWeekDays(weekNumber, year) {
		let { start } = this.getDateRangeOfWeek(weekNumber, year);
		let days = [];

		for (let i = 0; i < 7; i++) {
			let day = new Date(start.getFullYear(), start.getMonth(), start.getDate() + i);
			days.push(moment(day).format("YYYY-MM-DD"));
		}

		return days;
	}

	gatherBrokenWeeks(brokenWeeks) {
		let result = {};

		for (let week of brokenWeeks) {
			let index = week.periodIndex();

			if (!result[index]) {
				result[index] = [];
			}

			result[index].push(week);
		}

		return result;
	}

	reuniteGatheredBrokenWeeks(data) {
		let indexes = Object.keys(data);

		return indexes.map((index) => this.reuniteBrokenWeeks(index, data[index]));
	}

	reuniteBrokenWeeks(index, brokenWeeks) {
		if (brokenWeeks.length === 1) {
			// Can't be reunited
			return brokenWeeks[0];
		}

		let days = brokenWeeks.reduce((previous, current) => previous.concat(current.getDays()), []);

		days = days.map((day) => day.clone());

		let Week = require("./week"); // eslint-disable-line global-require
		return new Week({
			periodIndex: +index,
			days
		});
	}
}

module.exports = new Helper();
