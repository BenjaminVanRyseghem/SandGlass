const helper = require("./helper");

class Period {
	constructor({ periodIndex } = {}) {
		this._periodIndex = periodIndex;
	}

	periodIndex() { return this._periodIndex; }

	getName() {}

	isNull() { return false; }

	getYear() {
		return null;
	}

	getMonths() {
		return this.getYear().getMonths();
	}

	getWeeks() {
		// Needs to reunite broken weeks
		let weeks = this.getMonths().reduce((acc, month) => acc.concat(month.getWeeks()), []);

		let unbroken = weeks.filter((week) => !week.isBroken());
		let broken = weeks.filter((week) => week.isBroken());

		let gatheredWeeks = helper.gatherBrokenWeeks(broken);
		let reunitedWeeks = helper.reuniteGatheredBrokenWeeks(gatheredWeeks);

		return unbroken.concat(reunitedWeeks);
	}

	getDays() {
		return this.getMonths().reduce((acc, month) => acc.concat(month.getDays()), []);
	}

	containsYear(year) {
		return this.getYear().containsYear(year);
	}

	containsMonth(month) {
		return this.getMonths().some((each) => each.containsMonth(month));
	}

	containsWeek(week) {
		return this.getWeeks().some((each) => each.containsWeek(week));
	}

	containsDay(day) {
		return this.getDays().some((each) => each.containsDay(day));
	}

	accept(visitor) {
		return visitor.visitPeriod(this);
	}
}

module.exports = Period;
