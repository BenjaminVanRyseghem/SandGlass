const Period = require("./period");

class Year extends Period {
	constructor({ months }) {
		super(...arguments); // eslint-disable-line prefer-rest-params
		this._months = months;

		for (let month of this._months) {
			month.setYear(this);
		}
	}

	getName() {}

	getYear() {
		return this;
	}

	getMonths() {
		return this._months;
	}

	accept(visitor) {
		return visitor.visitYear(this);
	}

	containsYear(year) {
		return year === this.periodIndex();
	}
}

module.exports = Year;
