const helper = require("./helper");

class YearBuilder {
	buildYears(daysData) {
		let result = [];
		let years = {};

		// Gather all day per year
		for (let day of daysData) {
			let year = helper.getYearIndex(day);
			if (!years[year]) {
				years[year] = [];
			}
			years[year].push(day);
		}

		let keys = Object.keys(years);

		for (let year of keys) {
			result.push(this.build(years[year]));
		}

		return result;
	}

	build(daysData) {
		let Day = require("./day"); // eslint-disable-line global-require
		let Year = require("./year"); // eslint-disable-line global-require

		let currentYear = helper.getYearIndex(daysData[0]);

		let days = daysData.map((identifier) => {
			if (currentYear !== helper.getYearIndex(identifier)) {
				throw new Error("All days should be part of the same year");
			}

			return new Day({
				identifier,
				periodIndex: helper.getDayIndex(identifier).toString()
			});
		});

		let monthsData = this.buildMonthData(days);
		let months = this.linkPeriods(monthsData, currentYear);

		return new Year({
			months,
			periodIndex: currentYear.toString()
		});
	}

	buildMonthData = (days) => {
		let monthsData = {};

		// group per months
		for (let day of days) {
			let identifier = day.identifier();
			let dayMonthIndex = helper.getMonthIndex(identifier);
			let dayWeekIndex = helper.getWeekIndex(identifier);

			if (!monthsData[dayMonthIndex]) {
				monthsData[dayMonthIndex] = {
					weeks: {}
				};
			}

			let monthData = monthsData[dayMonthIndex];

			if (!monthData.weeks[dayWeekIndex]) {
				monthData.weeks[dayWeekIndex] = [];
			}

			monthData.weeks[dayWeekIndex].push(day);
		}

		return monthsData;
	};

	linkPeriods(monthsData, currentYear) {
		let Week = require("./week"); // eslint-disable-line global-require
		let BrokenWeek = require("./brokenWeek"); // eslint-disable-line global-require
		let Month = require("./month"); // eslint-disable-line global-require

		let months = [];

		let keys = Object.keys(monthsData);
		keys.forEach((monthIndex) => {
			let monthData = monthsData[monthIndex];
			let weeks = [];

			let wKeys = Object.keys(monthData.weeks);
			wKeys.forEach((weekIndex) => {
				let previousYear = false;
				if (parseInt(monthIndex, 10) === 1 && parseInt(weekIndex, 10) > 6) {
					previousYear = true;
				}

				let weekData = monthData.weeks[weekIndex];
				let yearToUse = previousYear ? currentYear - 1 : currentYear;

				let broken = helper.isWeekBroken(weekIndex, yearToUse);
				let ClassToUse = broken ? BrokenWeek : Week;

				if (previousYear) {
					weeks.unshift(new ClassToUse({
						days: weekData,
						periodIndex: weekIndex.toString()
					}));
				} else {
					weeks.push(new ClassToUse({
						days: weekData,
						periodIndex: weekIndex.toString()
					}));
				}
			});

			months.push(new Month({
				weeks,
				periodIndex: monthIndex.toString()
			}));
		});

		return months;
	}
}

module.exports = new YearBuilder();
