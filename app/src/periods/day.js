const Period = require("./period");

class Day extends Period {
	constructor({ week, identifier }) {
		super(...arguments); // eslint-disable-line prefer-rest-params

		this._week = week;
		this._identifier = identifier;
	}

	identifier() {
		return this._identifier;
	}

	getYear() {
		return this._week.getYear();
	}

	getMonths() {
		return this._week.getMonths();
	}

	// To use only once to set the back pointer
	setWeek(week) {
		if (this._week) {
			throw new Error("`this._week` has already been set!");
		}

		this._week = week;
	}

	getWeeks() {
		return [this._week];
	}

	getDays() {
		return [this];
	}

	accept(visitor) {
		return visitor.visitDay(this);
	}

	clone() {
		return new Day({
			identifier: this._identifier,
			periodIndex: this._periodIndex
		});
	}

	containsDay(otherDay) {
		return otherDay === this.identifier();
	}
}

module.exports = Day;
