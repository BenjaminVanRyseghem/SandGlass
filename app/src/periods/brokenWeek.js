const Period = require("./period");

class BrokenWeek extends Period {
	constructor({ month, days }) {
		super(...arguments); // eslint-disable-line prefer-rest-params

		this._month = month;
		this._days = days;

		for (let day of this._days) {
			day.setWeek(this);
		}
	}

	isBroken() {
		return true;
	}

	getYear() {
		return this._month.getYear();
	}

	// To use only once to set the back pointer
	setMonth(month) {
		if (this._month) {
			throw new Error("`this._month` has already been set!");
		}

		this._month = month;
	}

	getMonths() {
		return [this._month];
	}

	getWeeks() {
		return [this];
	}

	getDays() {
		return this._days;
	}

	accept(visitor) {
		return visitor.visitBrokenWeek(this);
	}

	containsWeek(week) {
		return week === this.periodIndex();
	}
}

module.exports = BrokenWeek;
