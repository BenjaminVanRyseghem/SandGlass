const json2csv = require("json2csv");
const { remote } = require("electron");
const { BrowserWindow, dialog } = remote;
const fs = require("fs");
const settings = remote.require("../src/settings");
const db = remote.require("../src/db");
const openExplorer = remote.require("open-file-explorer");

function smartTrim(string, maxLength) {
	if (!string) {
		return string;
	}
	if (maxLength < 1) {
		return string;
	}
	if (string.length <= maxLength) {
		return string;
	}
	if (maxLength === 1) {
		return `${string.substring(0, 1)}...`;
	}

	let midpoint = Math.ceil(string.length / 2);
	let toremove = string.length - maxLength;
	let lstrip = Math.ceil(toremove / 2);
	let rstrip = toremove - lstrip;
	return `${string.substring(0, midpoint - lstrip)}...${
		string.substring(midpoint + rstrip)}`;
}

window.setText = (element, text) => {
	element.innerText = smartTrim(text, 50);
	element.title = text;
};

window.changeDBFolder = () => {
	let win = BrowserWindow.fromId(window.windowId);
	let newPath = dialog.showOpenDialog(win, {
		properties: ["openDirectory"]
	});

	newPath.then(({ filePaths: [path] }) => {
		if (path) {
			settings.databaseFolder(path);
			window.setText(window.databaseFolder, `${settings.databaseFolder()}db.json`);
		}
	});
};

window.chooseAudioFile = () => {
	let win = BrowserWindow.fromId(window.windowId);
	let newPath = dialog.showOpenDialog(win, {
		filters: [
			{
				name: "Supported Audio Files",
				extensions: ["mp3", "wav"]
			},
			{
				name: "All Files",
				extensions: ["*"]
			}
		]
	});

	newPath.then(({ filePaths: [path] }) => {
		if (path) {
			settings.notificationSoundPath(path);
			window.setText(window.notificationSoundPath, settings.notificationSoundPath());
		}
	});
};

window.exportToCSV = () => {
	let win = BrowserWindow.fromId(window.windowId);
	let newPath = dialog.showSaveDialog(win, {
		filters: [
			{
				name: "Comma Separated Value",
				extensions: ["csv"]
			},
			{
				name: "All Files",
				extensions: ["*"]
			}
		]
	});

	if (newPath) {
		let data = db.getAllData();
		json2csv({
			data
		}, (err, csv) => {
			if (err) {
				dialog.showErrorBox("CSV Export", `The CSV export failed. The exact error was: \n\n${err}`);
			} else {
				fs.writeFileSync(newPath, csv, "utf8"); // eslint-disable-line no-sync
			}
		});
	}
};

window.projectToShowInTrayFn = (...args) => settings.projectToShowInTray(...args);
window.showTimerInTrayFn = (...args) => settings.showTimerInTray(...args);
window.showRemainingTimeFn = (...args) => settings.showRemainingTime(...args);
window.hideWhenStoppedFn = (...args) => settings.hideWhenStopped(...args);
window.databaseFolderFn = (...args) => settings.databaseFolder(...args);
window.notifyWhenReachingLimitFn = (...args) => settings.notifyWhenReachingLimit(...args);
window.playSoundWhenNotifyFn = (...args) => settings.playSoundWhenNotify(...args);
window.notificationSoundPathFn = (...args) => settings.notificationSoundPath(...args);
window.openExplorer = (...args) => openExplorer(...args);
