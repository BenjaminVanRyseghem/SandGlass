const electron = require("electron");
const { app } = electron;

const log = require("electron-log");
log.catchErrors();

const menu = require("./menu");
const Notification = require("./helper/notification.js");
const repl = require("./repl");
const settings = require("./settings");
const tickler = require("./tickler");
const time = require("./time");
const tray = require("./tray");
const { isMacOs } = require("./helper/os");

if (isMacOs()) {
	app.dock.hide();
}
app.on("ready", init);
app.on("window-all-closed", (error) => {
	error.preventDefault();
});

app.requestSingleInstanceLock();
app.on("second-instance", () => {});

function init() {
	initializeTray();
	repl.init();
	tickler.init();
	initializeNotification();
}

function initializeNotification() {
	tickler.onTick(() => {
		if (settings.notifyWhenReachingLimit()) {
			let dailyLimit = settings.dailyLimit();
			let limit = dailyLimit * 3600000; // hours to seconds
			let project = settings.projectToShowInTray();
			let duration = time.getTodayDurationFor(project);
			let ms = duration.asMilliseconds();
			let upperLimit = limit + tickler.duration();

			if (limit <= ms && ms < upperLimit) {
				const myNotification = new Notification({
					title: "Limit reached",
					body: `You just reached the ${dailyLimit}h limit. Congratulations!`,
					sound: settings.playSoundWhenNotify(),
					soundPath: settings.notificationSoundPath()
				});
				myNotification.show();
			}
		}
	});
}

function initializeTray() {
	let currentTray = tray.init();
	menu.init(currentTray);
}
