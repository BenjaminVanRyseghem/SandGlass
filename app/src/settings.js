const bridge = require("electron-settings");
const path = require("path");
const fs = require("fs");

const tickler = require("./tickler");

class Settings {
	showTimerInTray(boolean) {
		if (boolean === undefined) {
			return this._get("showTimerInTray");
		}
		let result = this._set("showTimerInTray", boolean);
		tickler.check({ showTimerInTray: boolean });
		return result;
	}

	showRemainingTime(boolean) {
		if (boolean === undefined) {
			return this._get("showRemainingTime");
		}
		return this._set("showRemainingTime", boolean);
	}

	projectToShowInTray(project) {
		if (project === undefined) {
			return this._get("projectToShowInTray") || "default";
		}
		let result = this._set("projectToShowInTray", project || undefined);
		require("./tray").updateTitle(); // eslint-disable-line global-require
		tickler.check({ project });
		return result;
	}

	hideWhenStopped(boolean) {
		if (boolean === undefined) {
			return this._get("hideWhenStopped") || false;
		}
		let result = this._set("hideWhenStopped", boolean);

		if (!tickler.isRunning()) {
			require("./tray").updateTitle(); // eslint-disable-line global-require
		}

		return result;
	}

	databaseFolder(newPath) {
		if (newPath === undefined) {
			let result = this._get("databaseFolder") || this._defaultDatabaseFolder();
			this._ensureSettingsFolderPath(result);

			return result;
		}
		let oldPath = this.databaseFolder();

		if (oldPath === newPath) {
			return null;
		}

		if (newPath[newPath.length - 1] !== path.sep) {
			newPath += path.sep; // eslint-disable-line no-param-reassign
		}

		let result = this._set("databaseFolder", newPath || undefined);
		this._ensureSettingsFolderPath(newPath);
		require("./db").migrate({ // eslint-disable-line global-require
			from: oldPath,
			to: newPath
		});

		return result;
	}

	dailyLimit(limit) {
		if (limit === undefined) {
			return this._get("dailyLimit") || 8;
		}
		let integer = +limit;
		if (isNaN(integer)) {
			throw new Error(`"${limit}" must be a number.`);
		}
		return this._set("dailyLimit", integer);
	}

	playSoundWhenNotify(playSound) {
		if (playSound === undefined) {
			return this._get("playSoundWhenNotify") || false;
		}
		return this._set("playSoundWhenNotify", playSound);
	}

	notificationSoundPath(notificationSoundPath) {
		if (notificationSoundPath === undefined) {
			return this._get("notificationSoundPath") || undefined;
		}
		return this._set("notificationSoundPath", notificationSoundPath ? notificationSoundPath : undefined);
	}

	notifyWhenReachingLimit(notify) {
		if (notify === undefined) {
			return this._get("notifyWhenReachingLimit") || false;
		}
		let result = this._set("notifyWhenReachingLimit", notify);
		tickler.check({ notifyWhenReachingLimit: notify });
		return result;
	}

	shouldStartTickler({
		notifyWhenReachingLimit,
		showTimerInTray
	} = {}) {
		return notifyWhenReachingLimit ||
			showTimerInTray ||
			this.showTimerInTray() ||
			this.notifyWhenReachingLimit();
	}

	_set(key, value, options) {
		return bridge.set(key, value, options);
	}

	_get(key) {
		return bridge.get(key);
	}

	_unset(key, options) {
		return bridge.unset(key, options);
	}

	_settingsPath() {
		return bridge.file();
	}

	_defaultDatabaseFolder() {
		let result = this._settingsPath();
		result = result.split(path.sep);
		result.pop();
		result.push("database");

		result = result.join(path.sep) + path.sep;

		return result;
	}

	_ensureSettingsFolderPath(settingsPath) {
		try {
			fs.accessSync(settingsPath, fs.F_OK); // eslint-disable-line no-sync
		} catch (error) {
			fs.mkdirSync(settingsPath); // eslint-disable-line no-sync
		}
	}
}

module.exports = new Settings();
