const electron = require("electron");
const path = require("path");
const about = require("./about");
const settings = require("./settings");
const db = require("./db");
const time = require("./time");
const info = require("./info");

const { BrowserWindow } = electron;
const { Menu: ElectronMenu } = electron;
const { app } = electron;

class Menu {
	constructor() {
		this._settingWindow = null;
	}

	init(tray) {
		tray.on("click", () => {
			let menuObject = ElectronMenu.buildFromTemplate(this._buildMenuTemplate());
			tray.popUpContextMenu(menuObject);
		});
	}

	_buildMenuTemplate() {
		let startStopLabel = this._isRunningForCurrentProject() ? "Stop" : "Start";
		let project = settings.projectToShowInTray();
		let remainingTime = time.getRemainingTime(project);
		let { time: end, over } = this._getEnd(project, remainingTime);
		let label = over ? `Ended at: ${end}` : `Ends at: ${end}`;

		let items = [
			{
				label,
				type: "normal",
				enabled: false
			},
			{
				type: "separator"
			},
			{
				label: `${startStopLabel} the clock`,
				click: this._toggleRunning.bind(this),
				enabled: true
			},
			{
				type: "separator"
			},
			{
				label: "Preferences",
				click: this._toggleSettings.bind(this),
				accelerator: "CmdOrCtrl+,",
				enabled: true
			},
			{
				label: "Refresh DB",
				click: this._refreshDB.bind(this),
				enabled: true
			},
			{
				label: `About ${info.name}`,
				click: about,
				enabled: true
			},
			{
				label: "Quit SandGlass",
				click: this._quit.bind(this),
				accelerator: "CmdOrCtrl+Q",
				enabled: true
			}
		];

		if (!settings.showTimerInTray()) {
			label = this._getDurationFor(project);

			if (settings.showRemainingTime()) {
				label += ` (${this._getRemainingTime(project, remainingTime)} left)`;
			}

			items.unshift({ type: "separator" });
			items.unshift({
				label,
				type: "normal",
				enabled: false
			});
		} else if (settings.showRemainingTime()) {
			items.unshift({
				label: `Remaining time: ${this._getRemainingTime(project, remainingTime)}`,
				type: "normal",
				enabled: false
			});
		}

		return items;
	}

	_getRemainingTime(project, remainingTime = time.getRemainingTime(project)) {
		return time.formatDuration(remainingTime);
	}

	_getEnd(project, remainingTime = time.getRemainingTime(project)) {
		let end = Date.now() + remainingTime;
		let over = false;

		if (time.isOverFor(project, remainingTime)) {
			over = true;
			end = time.getLastWorkedMoment(project);
		}

		return {
			time: time.formatTime(end),
			over
		};
	}

	_getDurationFor(project) {
		let duration = time.getTodayDurationFor(project);
		return time.formatDuration(duration);
	}

	_isRunningForCurrentProject() {
		let project = settings.projectToShowInTray();
		return db.isRunningFor(project);
	}

	_toggleRunning() {
		let project = settings.projectToShowInTray();
		if (this._isRunningForCurrentProject()) {
			db.stop(project);
		} else {
			db.start(project);
		}
	}

	_refreshDB() {
		db.refreshDatabase();
	}

	_toggleSettings() {
		if (this._settingWindow) {
			this._settingWindow.close();
			return;
		}

		this._settingWindow = new BrowserWindow({
			width: 640,
			height: 610,
			useContentSize: true,
			show: false,
			resizable: false,
			center: true,
			webPreferences: {
				preload: path.resolve(__dirname, "preloadSettings.js"),
				nodeIntegration: false,
				contextIsolation: false,
				enableRemoteModule: true
			}
		});

		this._settingWindow.on("closed", () => {
			this._settingWindow = null;
		});

		this._settingWindow.on("blur", () => {
			this._settingWindow.close();
		});

		this._settingWindow.loadURL(`file://${__dirname}/../resources/preferences.html`);
		this._settingWindow.webContents.on("did-finish-load", () => {
			this._settingWindow.webContents.executeJavaScript(`window.windowId = ${this._settingWindow.id};`);
			this._settingWindow.show();
		});

		this._settingWindow.focus();
	}

	_quit() {
		app.quit();
	}
}

module.exports = new Menu();
