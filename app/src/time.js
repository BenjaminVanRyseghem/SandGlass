const db = require("./db");
const settings = require("./settings");
const timeComputer = require("./time/timeComputer");
const helper = require("./periods/helper");

const moment = require("moment");
require("moment-duration-format");

class Time {
	getSegmentsForDay(project, day) {
		let records = db.getRecordsFor(project, day);

		if (!records.length) {
			return [];
		}

		if (records[records.length - 1].action !== "stop") {
			records.push({
				action: "stop",
				timestamp: Date.now(),
				project
			});
		}

		return timeComputer.computeWorkingSegmentsFor(records);
	}

	/**
	 * Used to get the worked time for a day once the day is over.
	 * Otherwise, use `getTodayDurationFor`.
	 *
	 * @param {string} project - Name of the project to track
	 * @param {string} day - Day to check
	 */
	getDurationForDay(project, day) {
		let segments = this.getSegmentsForDay(project, day);

		if (!segments.length) {
			return moment.duration(0);
		}

		let ms = timeComputer.computeTimeFromSegments(segments);

		return moment.duration(ms);
	}

	getDurationForWeek(project, weekNumber, year) {
		let weekDays = helper.getWeekDays(weekNumber, year);

		let result = moment.duration();

		for (let day of weekDays) {
			let duration = this.getDurationForDay(project, day);
			result.add(duration);
		}

		return result;
	}

	getLastWorkedMoment(project, day = this.formatDay(Date.now())) {
		let records = db.getRecordsFor(project, day);

		if (!records.length) {
			return Date.now();
		}

		if (records[records.length - 1].action === "start") {
			return Date.now();
		}

		return records[records.length - 1].timestamp;
	}

	getRemainingTime(project = settings.projectToShowInTray()) {
		let limit = settings.dailyLimit();
		let limitDuration = moment.duration(limit, "hours");
		limitDuration.add(1, "seconds");
		limitDuration.subtract(this.getTodayDurationFor(project));

		return limitDuration;
	}

	isOverFor(project, remainingTime = this.getRemainingTime(project)) {
		return remainingTime.as("milliseconds") < 0 && !db.isRunningFor(project);
	}

	getTodayDurationFor(project) {
		let day = this.formatDay(Date.now());

		return this.getDurationForDay(project, day);
	}

	formatDay(timestamp) { return moment(timestamp).format("YYYY-MM-DD"); }

	formatTime(time) { return moment(time).format("HH:mm:ss"); }

	formatDuration(duration, options) {
		let durationObject = moment.duration(duration);

		if (options && options.undotted) {
			return durationObject.format("hh mm ss", {
				trim: false
			});
		}
		return durationObject.format("hh:mm:ss", {
			trim: false
		});
	}
}

module.exports = new Time();
