const electron = require("electron");
const { Tray: ElectronTray } = electron;

const path = require("path");

const settings = require("./settings");
const time = require("./time");
const tickler = require("./tickler");
const db = require("./db");

class Tray {
	constructor() {
		this._dotted = true;
		this._tray = null;
	}

	init() {
		let pathToIcon = path.resolve(`${__dirname}/../resources/img/trayIconTemplate.png`);
		this._tray = new ElectronTray(pathToIcon);
		this._initTitle();

		return this._tray;
	}

	updateTitle() {
		if (!tickler.isRunning() && settings.hideWhenStopped()) {
			this._hideTitle();
			return;
		}

		if (settings.showTimerInTray()) {
			this._setDottedDurationTitle();
		} else {
			this._hideTitle();
		}
	}

	_hideTitle() {
		this._tray.setTitle("");
	}

	_setDottedDurationTitle() {
		let project = settings.projectToShowInTray();
		let title = this._getDurationFor(project, {
			undotted: false
		});

		this._tray.setTitle(title);
	}

	_initTitle() {
		this._initBlinkingTitle();
		tickler.onStart(() => (this._dotted = true));
		tickler.onStop(() => this.updateTitle());

		this.updateTitle();
	}

	_initBlinkingTitle() {
		tickler.onTick(() => {
			if (settings.showTimerInTray()) {
				let title = "";
				let project = settings.projectToShowInTray();

				if (db.isRunningFor(project)) {
					title = this._getDurationFor(project, {
						undotted: !this._dotted
					});
				} else {
					title = this._getDurationFor(project, {
						undotted: false
					});
				}

				this._tray.setTitle(title);

				this._dotted = !this._dotted;
			}
		});
	}

	_getDurationFor(project, options) {
		let undotted = options && options.undotted;
		let duration = time.getTodayDurationFor(project);
		return time.formatDuration(duration, { undotted });
	}
}

module.exports = new Tray();
