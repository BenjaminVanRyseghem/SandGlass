const ComputingState = require("./computingState");
const EndState = require("./endState");

class InactiveState extends ComputingState {
	compute(record) {
		if (record === null) {
			return new EndState({
				segments: this._segments
			});
		}

		if (record.action === "start") {
			let ActiveState = require("./activeState"); // eslint-disable-line global-require
			return new ActiveState({
				segments: this._segments,
				startingTime: record.timestamp
			});
		}

		return this;
	}
}

module.exports = InactiveState;
