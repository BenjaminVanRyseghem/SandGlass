const ActiveState = require("./activeState");

class TimeComputer {
	computeWorkingSegmentsFor(records) {
		if (records[0].action !== "start") {
			throw new Error("The day should begin with a \"start\" record");
		}

		if (records[records.length - 1].action !== "stop") {
			throw new Error("The day should end with a \"stop\" record");
		}

		/*
		 * Append `null` at the end as ending flag, and remove the head
		 * as the iteration can start on the second element
		 */
		let allRecords = records.slice(1);
		allRecords.push(null);

		let currentState = new ActiveState({
			startingTime: records[0].timestamp
		});

		for (let each of allRecords) {
			currentState = currentState.compute(each);
		}

		return currentState.getResult();
	}

	computeWorkingTimeFor(records) {
		let segments = this.computeWorkingSegmentsFor(records);
		return this.computeTimeFromSegments(segments);
	}

	computeTimeFromSegments(segments) {
		return segments.reduce((previous, current) => previous + current.delta(), 0);
	}
}

module.exports = new TimeComputer();
