class ComputingState {
	constructor({ segments = [] } = {}) {
		this._segments = segments;
	}

	compute(record) { // eslint-disable-line no-unused-vars
		throw new Error("Should be overridden");
	}

	_exit() {
		return this._segments;
	}
}

module.exports = ComputingState;
