class Segment {
	constructor({ start, end }) {
		this._start = start;
		this._end = end;
	}

	start() {
		return this._start;
	}

	end() {
		return this._end;
	}

	delta() {
		return this._end - this._start;
	}
}

module.exports = Segment;
