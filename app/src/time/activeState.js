const ComputingState = require("./computingState");
const Segment = require("./segment");

class ActiveState extends ComputingState {
	constructor({ startingTime }) {
		super(...arguments); // eslint-disable-line prefer-rest-params
		this._startingTime = startingTime;
	}

	compute(record) {
		if (record.action === "stop") {
			let newSegment = new Segment({
				start: this._startingTime,
				end: record.timestamp
			});

			this._segments.push(newSegment);

			let InactiveState = require("./inactiveState"); // eslint-disable-line global-require
			return new InactiveState({
				segments: this._segments
			});
		}

		return this;
	}
}

module.exports = ActiveState;
