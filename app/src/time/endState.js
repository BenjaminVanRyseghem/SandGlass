const ComputingState = require("./computingState");

class EndState extends ComputingState {
	getResult() {
		return this._segments;
	}
}

module.exports = EndState;
