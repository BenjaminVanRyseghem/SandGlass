class Tickler {
	constructor() {
		this._duration = 1000;
		this._onTickCallbacks = [];
		this._onStartCallbacks = [];
		this._onStopCallbacks = [];
		this._running = false;
		this._timeout = null;
	}

	init() {
		this.check();
	}

	onTick(callback) { return this._onTickCallbacks.push(callback); }

	onStart(callback) { return this._onStartCallbacks.push(callback); }

	onStop(callback) { return this._onStopCallbacks.push(callback); }

	duration() {
		return this._duration;
	}

	isRunning() {
		return this._running;
	}

	start() {
		if (this._running) {
			return;
		}

		this._running = true;
		this._applyOnStart();
		this._tick();
	}

	stop() {
		if (!this._running) {
			return;
		}

		this._running = false;
		clearTimeout(this._timeout);
		this._applyOnStop();
	}

	check({ project, notifyWhenReachingLimit, showTimerInTray } = {}) {
		let settings = require("./settings"); // eslint-disable-line global-require
		let projectToCheck = project || settings.projectToShowInTray();
		let db = require("./db"); // eslint-disable-line global-require
		if (db.isRunningFor(projectToCheck) && settings.shouldStartTickler({
			notifyWhenReachingLimit,
			showTimerInTray
		})) {
			this.start();
		} else {
			this.stop();
		}
	}

	_tick() {
		this._applyOnTick();
		this._timeout = setTimeout(this._tick.bind(this), this._duration);
	}

	_applyOnTick() {
		for (let fn of this._onTickCallbacks) {
			fn();
		}
	}

	_applyOnStart() {
		for (let fn of this._onStartCallbacks) {
			fn();
		}
	}

	_applyOnStop() {
		for (let fn of this._onStopCallbacks) {
			fn();
		}
	}
}

module.exports = new Tickler();
