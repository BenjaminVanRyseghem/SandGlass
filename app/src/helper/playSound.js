const path = require("path");
const player = require("play-sound")();

const isDev = process.mainModule.filename.indexOf("app.asar") === -1;

const defaultPath = isDev ? path.resolve(__dirname, "audio.mp3") : path.resolve(process.resourcesPath, "audio.mp3");

module.exports = (soundPath = defaultPath) => {
	player.play(soundPath, (err) => {
		if (err) {
			throw err;
		}
	});
};
