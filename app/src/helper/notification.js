const { Notification: ElectronNotification } = require("electron");
const playSound = require("./playSound.js");

module.exports = class Notification {
	constructor({ title, body, sound, soundPath }) {
		this._sound = sound;
		this._soundPath = soundPath;

		this._notification = new ElectronNotification({
			title,
			body
		});
	}

	show() {
		this._notification.show();
		if (this._sound) {
			playSound(this._soundPath);
		}
	}
};
