const os = require("os");

function isMacOs() {
	return os.type() === "Darwin";
}

module.exports = {
	isMacOs
};
