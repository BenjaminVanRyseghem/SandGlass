const moment = require("moment");
const low = require("lowdb");
const fs = require("fs");
const storage = require("lowdb/file-sync");

const settings = require("./settings");
const tickler = require("./tickler");

const { dialog } = require("electron");

class DB {
	constructor() {
		this.refreshDatabase();
	}

	/**
	 * Start the timer for the provided project.
	 *
	 * @param {string} project - Project to start
	 */
	start(project = settings.projectToShowInTray()) {
		if (project === settings.projectToShowInTray() && settings.shouldStartTickler()) {
			tickler.start();
		}

		let date = require("./time").formatDay(Date.now()); // eslint-disable-line global-require
		this._database(date).push({
			project,
			action: "start",
			timestamp: Date.now(),
			time: moment(Date.now()).format("YYYY-MM-DD HH:mm:ss")
		});
	}

	refreshDatabase() {
		try {
			this._database = low(`${settings.databaseFolder()}db.json`, { storage });
		} catch (error) {
			dialog.showErrorBox("Error while opening the this._database", `The this._database file seems to be corrupted: ${settings.databaseFolder()}db.json`);
			this._database = low();
		}
	}

	/**
	 * Stop the timer for the provided project.
	 *
	 * @param {string} project - Project to stop
	 */
	stop(project = settings.projectToShowInTray()) {
		if (project === settings.projectToShowInTray()) {
			tickler.stop();
		}

		let date = require("./time").formatDay(Date.now()); // eslint-disable-line global-require
		this._database(date).push({
			project,
			action: "stop",
			timestamp: Date.now(),
			time: moment(Date.now()).format("YYYY-MM-DD HH:mm:ss")
		});
	}

	/**
	 * Return all the records for the provided day and project.
	 * The records are sorted based on their timestamp.
	 *
	 * @param {string} project - Project to retrieve
	 * @param {Day} day - Day to compute
	 * @returns {object[]}
	 */
	getRecordsFor(project = "default", day) {
		if (!day) {
			throw new Error("`day` is mandatory");
		}

		return this._database(day)
			.chain()
			.filter({ project })
			.sortBy("timestamp")
			.value()
			.slice();
	}

	isRunningFor(project) {
		let day = require("./time").formatDay(Date.now()); // eslint-disable-line global-require
		let records = this.getRecordsFor(project, day);

		if (!records.length) {
			return false;
		}

		let last = records[records.length - 1];
		return last.action === "start";
	}

	getAllData() {
		let data = [];

		let keys = Object.keys(this._database.object);

		for (let key of keys) {
			let value = this._database.object[key];
			data = data.concat(value);
		}

		return data;
	}

	getAllDays() {
		return Object.keys(this._database.object).filter((day) => this._database.object[day].length);
	}

	projectsFor(day) {
		let data = this._database.object[day];

		let projects = [];

		for (let datum of data) {
			if (projects.indexOf(datum.project) === -1) {
				projects.push(datum.project);
			}
		}

		return projects;
	}

	migrate(options) {
		let { from } = options;
		let { to } = options;

		fs.renameSync(`${from}db.json`, `${to}db.json`); // eslint-disable-line no-sync

		this.refreshDatabase();
	}
}

module.exports = new DB();
