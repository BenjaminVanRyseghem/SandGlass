/* eslint max-lines: [2, 400] */
const electron = require("electron");
const { app } = electron;

const settings = require("./settings");
const db = require("./db");
const info = require("./info");
const time = require("./time");
const helper = require("./periods/helper");

const net = require("net");
const nodeREPL = require("repl");
const fs = require("fs");
const spawnargs = require("spawn-args");
const commandLineArgs = require("command-line-args");
const ansi = require("ansi-styles");
const Table = require("cli-table");

const numberOfWorkedDayPerWeek = 5;

const cliOptions = [
	{
		name: "help",
		alias: "h",
		type: Boolean,
		defaultOption: true,
		description: "Display this text"
	},
	{
		name: "version",
		alias: "v",
		type: Boolean,
		description: "Display the version text"
	},

	{
		name: "start",
		alias: "s",
		type: Boolean,
		description: "Start the clock"
	},
	{
		name: "stop",
		alias: "S",
		type: Boolean,
		description: "Stop the clock"
	},
	{
		name: "day",
		alias: "d",
		type: Boolean,
		description: "Display time spent on the current project today"
	},
	{
		name: "week",
		alias: "w",
		type: Boolean,
		description: "Display time spent on the current project this week"
	},
	{
		name: "option",
		alias: "o",
		type: String,
		description: "Get an option. Set it if --value is used"
	},
	{
		name: "project",
		alias: "p",
		type: String,
		description: "Display the current project"
	},
	{
		name: "value",
		type: String,

		// multiple: true,
		description: "New value of an option"
	},
	{
		name: "period",
		alias: "P",
		type: String,
		description: "Specify a period to show the time spent (format \"YYYY-MM-DD\")"
	},
	{
		name: "verbose",
		alias: "V",
		type: Boolean,
		description: "Make output more verbose"
	}
];

const usageOptions = {
	title: info.name,
	description: info.description,
	synopsis: [
		"$ sand-glass [bold]{--start} [[bold]{--project} [underline]{projectName}]",
		"$ sand-glass [bold]{--option}",
		"$ sand-glass [bold]{--option} [bold]{--value} [underline]{newValue}",
		"$ sand-glass [bold]{--help}"
	]
};

class REPL {
	constructor() {
		this._cli = commandLineArgs(cliOptions);
	}

	init() {
		let sockets = [];
		let replServer = null;

		let server = net.createServer((socket) => {
			sockets.push(socket);

			replServer = nodeREPL.start({
				prompt: "",
				input: socket,
				output: socket,
				eval: this._evalCmd.bind(this)
			});

			replServer.on("exit", () => {
				socket.end();
			});
		});

		try {
			fs.unlinkSync("/tmp/sand-glass-sock"); // eslint-disable-line no-sync
		} catch (error) {
			// Absorb errors
		}

		server.listen("/tmp/sand-glass-sock");

		app.on("quit", () => {
			for (let socket of sockets) {
				socket.end();
			}

			if (replServer) {
				replServer.close();
			}

			server.close();
		});
	}

	_evalCmd(cmd, context, filename, callback) { // eslint-disable-line max-params,max-statements
		let args = spawnargs(cmd.trim());
		args = args.map((value) => value.replace(/^['"]|['"]$/g, ""));

		let options = {};

		try {
			options = this._cli.parse(args);
		} catch (error) {
			if (error.name === "UNKNOWN_OPTION") {
				let output = this._cli.getUsage(usageOptions);
				callback(null, output);
				return;
			}
			callback(error);
			return;
		}

		if (args[0] === "" || options.help) {
			let output = this._cli.getUsage(usageOptions);
			callback(null, output);
			return;
		}

		if (options.version) {
			let output = info.longVersion;
			callback(null, output);
			return;
		}

		let result = this._dispatchAction(options);
		callback(null, result);
	}

	_dispatchAction(options) {
		if (options.start) {
			return this._start(options.project, options);
		}

		if (options.stop) {
			return this._stop(options.project, options);
		}

		if (options.day) {
			return this._showDayTime(options.project, options.period, options);
		}

		if (options.week) {
			return this._showWeekTime(options.project, options.period, options);
		}

		if (options.option) {
			return this._setSettings(options);
		}

		if (Object.hasOwnProperty(options, "project")) { // eslint-disable-line no-prototype-builtins
			if (options.value) {
				return this._setSettings({
					option: "projectToShowInTray",
					value: options.value
				});
			}
			return settings.projectToShowInTray();
		}

		return "";
	}

	_start(project, options) {
		db.start(project);

		let startTime = options.verbose ? ` at ${this._ansiWrap("underline", time.formatTime(Date.now()))}` : "";

		if (project) {
			return `Clock started for the project ${this._ansiWrap("bold", project)}${startTime}`;
		}
		return `Clock started${startTime}`;
	}

	_stop(project, options) {
		db.stop(project);

		let stopTime = options.verbose ? ` at ${this._ansiWrap("underline", time.formatTime(Date.now()))}` : "";

		if (project) {
			return `Clock stopped for the project ${this._ansiWrap("bold", project)}${stopTime}`;
		}
		return `Clock stopped${stopTime}`;
	}

	_showDayTime(project = settings.projectToShowInTray(), period = time.formatDay(Date.now()), options) {
		let duration = time.getDurationForDay(project, period);
		let totalTime = time.formatDuration(duration);
		let dailyLimit = time.formatDuration(settings.dailyLimit() * 3600000);

		if (!options.verbose) {
			return `${totalTime} / ${dailyLimit}`;
		}

		let segments = time.getSegmentsForDay(project, period);

		let result = `${this._ansiWrap("bold", period)}: ${totalTime} / ${dailyLimit}`;

		if (segments.length) {
			result += "\n\n";
		}

		let data = segments.map((segment) => [
			time.formatTime(segment.start()),
			time.formatTime(segment.end()),
			time.formatDuration(segment.delta())
		]);

		result += this._buildTable(["From", "To", "Time"], data);
		return result;
	}

	_showWeekTime(project = settings.projectToShowInTray(), period = time.formatDay(Date.now())) {
		let weekNumber = helper.getWeekIndex(period);
		let year = helper.getYearIndex(period);

		let duration = time.getDurationForWeek(project, weekNumber, year);

		let dailyLimit = time.formatDuration(settings.dailyLimit() * 3600000 * numberOfWorkedDayPerWeek);
		return `${time.formatDuration(duration)} / ${dailyLimit}`;
	}

	_setSettings(options) {
		let key = options.option;

		let fn = settings[key];

		if (!fn) {
			return `Unknown setting "${key}"`;
		}

		let message = "";

		if (options.value) {
			let args = this._sanitizeValue(options.value);
			try {
				fn.call(settings, args);
				message = `Option ${key} set to ${args}`;
			} catch (error) {
				message = error.message;
			}
		} else {
			message = fn.call(settings);
		}

		return message;
	}

	_sanitizeValue(value) {
		if (!isNaN(value)) {
			return +value;
		}

		switch (value.toLowerCase()) {
			case "true":
				return true;
			case "false":
				return false;
			case "undefined":
				return undefined;
			case "null":
				return null;
			default:
				return value;
		}
	}

	_ansiWrap(style, string) {
		return ansi[style].open + string + ansi[style].close;
	}

	_buildTable(headers, data) {
		let table = new Table({
			chars: {
				top: "═",
				"top-mid": "╤",
				"top-left": "╔",
				"top-right": "╗",
				bottom: "═",
				"bottom-mid": "╧",
				"bottom-left": "╚",
				"bottom-right": "╝",
				left: "║",
				"left-mid": "╟",
				mid: "─",
				"mid-mid": "┼",
				right: "║",
				"right-mid": "╢",
				middle: "│"
			},
			colAligns: ["middle", "middle", "middle"],
			style: {
				head: ["bold", "blue"]
			},
			head: headers
		});

		table.push(...data);

		return table.toString();
	}
}

module.exports = new REPL();
