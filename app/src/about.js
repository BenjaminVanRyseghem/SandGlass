/* eslint-disable camelcase */
const path = require("path");
const { nativeTheme } = require("electron");
const openAboutWindow = require("about-window").default;

module.exports = () => {
	let cssPath = undefined; // eslint-disable-line no-undef-init
	if (nativeTheme.shouldUseDarkColors) {
		cssPath = path.resolve(__dirname, "darkMode.css");
	}
	openAboutWindow({
		product_name: "SandGlass",
		css_path: cssPath,
		icon_path: path.resolve(__dirname, "..", "resources", "img", "icon.png"),
		copyright: "Copyright © 2016 Benjamin Van Ryseghem",
		description: "Logs your time. Keeps your data safe.",
		homepage: "https://gitlab.com/BenjaminVanRyseghem/SandGlass/",
		bug_report_url: "https://gitlab.com/BenjaminVanRyseghem/SandGlass/issues/new",
		bug_link_text: "Report a bug",
		win_options: {
			resizable: false
		},
		use_version_info: true
	});
};
