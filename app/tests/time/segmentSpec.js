const Segment = require("../../src/time/segment");

describe("time/computingState", () => {
	it("return start time correctly", () => {
		const start = {};
		let segment = new Segment({
			start
		});

		expect(segment.start()).toBe(start);
	});

	it("return end time correctly", () => {
		const end = {};
		let segment = new Segment({
			end
		});

		expect(segment.end()).toBe(end);
	});

	it("compute delta correctly", () => {
		const start = 1000;
		const end = 2000;
		let segment = new Segment({
			start,
			end
		});

		expect(segment.delta()).toBe(1000);
	});
});
