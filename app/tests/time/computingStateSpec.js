const ComputingState = require("../../src/time/computingState");

describe("time/computingState", () => {
	const segments = {};

	let state = null;

	beforeEach(() => {
		state = new ComputingState({
			segments
		});
	});

	it("can not compute", () => {
		expect(state.compute.bind(state).bind(state)
			.bind(state)).toThrowError(Error, "Should be overridden");
	});

	it("can compute", () => {
		expect(state._exit()).toBe(segments); // eslint-disable-line no-underscore-dangle
	});
});
