class YearVisitor {
	constructor() {
		this._numberOfMonths = 0;
		this._numberOfUnbrokenWeeks = 0;
		this._numberOfBrokenWeeks = 0;
		this._numberOfDays = 0;
	}

	visitYear(year) {
		year.getMonths().forEach((month) => month.accept(this));
	}

	visitWeeks(year) {
		year.getWeeks().forEach((week) => week.accept(this));
	}

	visitMonth(month) {
		this._numberOfMonths++;
		month.getWeeks().forEach((week) => week.accept(this));
	}

	visitWeek(week) {
		this._numberOfUnbrokenWeeks++;
		week.getDays().forEach((days) => days.accept(this));
	}

	visitBrokenWeek(week) {
		this._numberOfBrokenWeeks++;
		week.getDays().forEach((days) => days.accept(this));
	}

	visitDay(day) { // eslint-disable-line no-unused-vars
		this._numberOfDays++;
	}

	getNumberOfDays() { return this._numberOfDays; }

	getNumberOfUnbrokenWeeks() { return this._numberOfUnbrokenWeeks; }

	getNumberOfBrokenWeeks() { return this._numberOfBrokenWeeks; }

	getNumberOfMonths() { return this._numberOfMonths; }
}

module.exports = YearVisitor;
