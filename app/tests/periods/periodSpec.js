const Period = require("../../src/periods/period");

describe("periods/period", () => {
	let period = null;
	beforeEach(() => {
		period = new Period();
	});

	it("has no year", () => {
		expect(period.getYear()).toBe(null);
	});

	it("delegate the fetch of months to the year", () => {
		let fakeGetMonths = jest.fn();
		jest.spyOn(period, "getYear").mockImplementation(() => ({ getMonths: fakeGetMonths }));

		period.getMonths();

		expect(period.getYear).toHaveBeenCalled();

		expect(fakeGetMonths).toHaveBeenCalled();
	});

	it("delegate to the year to resolve `containsYear`", () => {
		let fakeContainsYear = jest.fn();
		jest.spyOn(period, "getYear").mockImplementation(() => ({ containsYear: fakeContainsYear }));

		period.containsYear("2016");

		expect(period.getYear).toHaveBeenCalled();

		expect(fakeContainsYear).toHaveBeenCalledWith("2016");
	});

	it("accept a visitor by calling `visitPeriod`", () => {
		let visitPeriod = jest.fn();
		let visitor = { visitPeriod };

		period.accept(visitor);

		expect(visitPeriod).toHaveBeenCalledWith(period);
	});
});
