$(document).ready(() => {
	initializeForm();
	initializeHooks();

	$(document).bind("keydown", "meta+,", () => {
		window.close();
	});
});

function initializeCheckboxes() {
	let showRemainingTimeValue = window.showRemainingTimeFn();
	let hideWhenStoppedValue = window.hideWhenStoppedFn();
	let notifyWhenReachingLimitValue = window.notifyWhenReachingLimitFn();
	let playSoundWhenNotifyValue = window.playSoundWhenNotifyFn();
	let showTimerInTrayValue = window.showTimerInTrayFn();

	if (showTimerInTrayValue) {
		window.showTimerInTray.setAttribute("checked", true);
	}

	if (showRemainingTimeValue) {
		window.showRemainingTime.setAttribute("checked", true);
	}

	if (hideWhenStoppedValue) {
		window.hideWhenStopped.setAttribute("checked", true);
	}

	if (notifyWhenReachingLimitValue) {
		window.showNotification.setAttribute("checked", true);
	}

	if (playSoundWhenNotifyValue) {
		window.playSound.setAttribute("checked", true);
	}
}

function initializeForm() {
	let projectToShowInTrayValue = window.projectToShowInTrayFn();
	let databaseFolderValue = window.databaseFolderFn();
	let notificationSoundPathValue = window.notificationSoundPathFn();

	initializeCheckboxes();

	window.setText(window.projectToShowInTray, projectToShowInTrayValue);
	window.setText(window.databaseFolder, `${databaseFolderValue}db.json`);
	window.setText(window.notificationSoundPath, notificationSoundPathValue || "Default value");
}

function initializeHooks() {
	$("#showTimerInTray").change(() => {
		let value = $("#showTimerInTray").is(":checked");
		window.showTimerInTrayFn(value);
	});

	$("#showRemainingTime").change(() => {
		let value = $("#showRemainingTime").is(":checked");
		window.showRemainingTimeFn(value);
	});

	$("#hideWhenStopped").change(() => {
		let value = $("#hideWhenStopped").is(":checked");
		window.hideWhenStoppedFn(value);
	});

	$("#projectToShowInTray").keyup(() => {
		let value = $("#projectToShowInTray").val();
		window.projectToShowInTrayFn(value);
	});

	$("#change-db-folder").click(window.changeDBFolder);
	$("#reveal-db-folder").click(() => {
		window.openExplorer(window.databaseFolderFn());
	});
	$("#choose-audio-file").click(window.chooseAudioFile);
	$("#reset-audio-file").click(() => {
		window.notificationSoundPathFn(null);
		window.setText(window.notificationSoundPath, "Default value");
	});

	$("#exportToCSV").click(window.exportToCSV);

	$("#showNotification").change(() => {
		let value = $("#showNotification").is(":checked");
		window.notifyWhenReachingLimitFn(value);
	});

	$("#playSound").change(() => {
		let value = $("#playSound").is(":checked");
		window.playSoundWhenNotifyFn(value);
	});
}
