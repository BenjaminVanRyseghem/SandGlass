# SandGlass

| Branch | Status |
--------|--------
| `master` | [![build status](https://gitlab.com/BenjaminVanRyseghem/SandGlass/badges/master/build.svg)](https://gitlab.com/BenjaminVanRyseghem/SandGlass/commits/master) |
| `develop` | [![build status](https://gitlab.com/BenjaminVanRyseghem/SandGlass/badges/develop/build.svg)](https://gitlab.com/BenjaminVanRyseghem/SandGlass/commits/develop) |

An electron-based time tracker

# How to

```
yarn install
yarn run preprod:local
```

The `dmg` file will be located in the `dist` directory.
The `app` file will be located in the `release/osx` directory.

# License

Copyright :copyright: 2016-2018 Benjamin Van Ryseghem <benjamin@vanryseghem.com>

The code in this repository is distributed under the [GPL-3.0](http://www.gnu.org/licenses/gpl-3.0.en.html) license. See [LICENSE](LICENSE) for additional information.
